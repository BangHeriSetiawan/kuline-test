package com.simxdeveloper.kulinatest.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.simxdeveloper.kulinatest.R;
import com.simxdeveloper.kulinatest.data.MenuDayLangganan;
import java.util.List;

/**
 * User: simx Date: 13/05/18 16:35
 */
public class AdapterDayLangganan extends RecyclerView.Adapter<AdapterDayLangganan.Holder> {
  private List<MenuDayLangganan> dayLangganans;

  public AdapterDayLangganan (
      List<MenuDayLangganan> dayLangganans) {
    this.dayLangganans = dayLangganans;
  }

  @NonNull
  @Override
  public Holder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
    View view  = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_day_langganan,parent,false);
    return new Holder (view);
  }

  @Override
  public void onBindViewHolder (@NonNull Holder holder, int position) {
    MenuDayLangganan dayLangganan = getMenu (position);
    holder.tvPrice.setText (dayLangganan.getPrice ());
    holder.tvDay.setText (dayLangganan.getName ());
  }
  private MenuDayLangganan getMenu(int pos){
    return dayLangganans.get (pos);
  }
  public void updateData(List<MenuDayLangganan> dayLangganans){
    this.dayLangganans = dayLangganans;
    notifyDataSetChanged ();
  }
  @Override
  public int getItemCount () {
    return dayLangganans.size ();
  }

  public class Holder extends RecyclerView.ViewHolder {
    TextView tvDay;
    TextView tvPrice;
    public Holder (View itemView) {
      super (itemView);
      tvDay = itemView.findViewById (R.id.tv_day);
      tvPrice = itemView.findViewById (R.id.tv_price);
    }
  }
}
