package com.simxdeveloper.kulinatest.ui.steper;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.simxdeveloper.kulinatest.R;
import com.simxdeveloper.kulinatest.data.MenuDayLangganan;
import com.simxdeveloper.kulinatest.ui.AdapterDayLangganan;
import com.squareup.timessquare.CalendarPickerView;
import com.squareup.timessquare.CalendarPickerView.OnDateSelectedListener;
import com.squareup.timessquare.CalendarPickerView.SelectionMode;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout.OnBackClickedCallback;
import com.stepstone.stepper.StepperLayout.OnCompleteClickedCallback;
import com.stepstone.stepper.StepperLayout.OnNextClickedCallback;
import com.stepstone.stepper.VerificationError;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class LanggananFragment extends Fragment implements BlockingStep {


  @BindView(R.id.rcv_langganan_day)
  RecyclerView rcvLanggananDay;
  Unbinder unbinder;
  @BindView(R.id.tv_box)
  TextView tvBox;
  @BindView(R.id.calender_view_select_day)
  CalendarPickerView calenderViewSelectDay;
  private  int totalBox = 1;
  public LanggananFragment () {
  }

  private ArrayList<MenuDayLangganan> menuDayLangganans = new ArrayList<> ();
  private AdapterDayLangganan adapterDayLangganan;

  public static LanggananFragment newInstance () {
    Bundle args = new Bundle ();
    LanggananFragment fragment = new LanggananFragment ();
    fragment.setArguments (args);
    return fragment;
  }

  @Override
  public View onCreateView (LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate (R.layout.fragment_langganan, container, false);
    unbinder = ButterKnife.bind (this, view);
    initBoxValue();
    adapterDayLangganan = new AdapterDayLangganan (new ArrayList<MenuDayLangganan> ());
    menuDayLangganans.add (new MenuDayLangganan ("", "5 Hari", "Rp 25.000/hari"));
    menuDayLangganans.add (new MenuDayLangganan ("", "10 Hari", "Rp 25.000/hari"));
    menuDayLangganans.add (new MenuDayLangganan ("", "20 Hari", "Rp 25.000/hari"));
    menuDayLangganans.add (new MenuDayLangganan ("", "Pilih Sendiri", "Min 2 hari"));
    return view;
  }

  /**
   * Sample init value in box
   */
  private void initBoxValue () {
    tvBox.setText (""+totalBox+" Box");
  }

  @Override
  public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated (view, savedInstanceState);
    final Calendar nextYear = Calendar.getInstance();
    nextYear.add(Calendar.YEAR, 1);
    final Calendar lastYear = Calendar.getInstance();
    lastYear.add(Calendar.YEAR, -1);
    Date today = new Date ();
    calenderViewSelectDay
        .init (lastYear.getTime (),nextYear.getTime ())
        .inMode (SelectionMode.MULTIPLE).withSelectedDate (new Date ());


    adapterDayLangganan.updateData (menuDayLangganans);
    rcvLanggananDay.setHasFixedSize (true);
    rcvLanggananDay.setItemAnimator (new DefaultItemAnimator ());
    rcvLanggananDay.setLayoutManager (new GridLayoutManager (getContext (), 2));
    rcvLanggananDay.setAdapter (adapterDayLangganan);
  }

  @Override
  public void onResume () {
    super.onResume ();
    if (menuDayLangganans.size ()!=0){
      menuDayLangganans.removeAll (new ArrayList<> ());
    }
  }

  @Override
  public void onNextClicked (OnNextClickedCallback callback) {
    callback.goToNextStep ();
  }

  @Override
  public void onCompleteClicked (OnCompleteClickedCallback callback) {
    callback.complete ();
  }

  @Override
  public void onBackClicked (OnBackClickedCallback callback) {
    callback.goToPrevStep ();
  }

  @Nullable
  @Override
  public VerificationError verifyStep () {
    return null;
  }

  @Override
  public void onSelected () {

  }

  @Override
  public void onError (@NonNull VerificationError error) {

  }

  @Override
  public void onDestroyView () {
    super.onDestroyView ();
    unbinder.unbind ();
  }



  /**
   * Action Minus box
   */
  @OnClick(R.id.tv_min)
  public void onTvMinClicked () {
    totalBox = totalBox - 1;
    tvBox.setText (""+totalBox+" Box");
  }

  /**
   * Action Plus box
   */
  @OnClick(R.id.tv_plus)
  public void onTvPlusClicked () {
    totalBox = totalBox + 1;
    tvBox.setText (""+totalBox+" Box");
  }
}
