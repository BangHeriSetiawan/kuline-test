package com.simxdeveloper.kulinatest.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.simxdeveloper.kulinatest.ui.steper.LanggananFragment;
import com.simxdeveloper.kulinatest.ui.steper.PembayaranFragment;
import com.simxdeveloper.kulinatest.ui.steper.PengirimanFragment;
import com.simxdeveloper.kulinatest.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.StepperLayout.StepperListener;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
import com.stepstone.stepper.viewmodel.StepViewModel.Builder;


public class MainActivity extends AppCompatActivity {

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.stepperLayout)
  StepperLayout stepperLayout;
  private AdapterSteper adapterSteper;

  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.activity_main);
    ButterKnife.bind (this);
    adapterSteper = new AdapterSteper (getSupportFragmentManager (), this);
    initActionBar ();
    initSteper ();

  }


  private void initSteper () {
    stepperLayout = findViewById (R.id.stepperLayout);
    stepperLayout.setListener (stepperListener);
    stepperLayout.setAdapter (adapterSteper);
  }

  /**
   * Create Actionbar
   */
  private void initActionBar () {
    setSupportActionBar (toolbar);
    getSupportActionBar ().setDisplayShowTitleEnabled (true);
    getSupportActionBar ().setHomeButtonEnabled (true);
    getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
    getSupportActionBar ().setHomeAsUpIndicator (getDrawable (R.drawable.ic_arrow_back_white_24dp));
  }

  /**
   * Listerner for steper layour
   */
  private StepperListener stepperListener = new StepperListener () {
    @Override
    public void onCompleted (View completeButton) {

    }

    @Override
    public void onError (VerificationError verificationError) {

    }

    @Override
    public void onStepSelected (int newStepPosition) {
      Log.e ("MainActivity", "onStepSelected: " + newStepPosition);
      switch (newStepPosition) {
        case 0:
          getSupportActionBar ().setTitle ("Mulai");
          break;
        case 1:
          getSupportActionBar ().setTitle ("Pengiriman");
          break;
        case 2:
          getSupportActionBar ().setTitle ("Pembayaran");
          break;
      }

    }

    @Override
    public void onReturn () {

    }
  };

  /**
   * Adapter for fragment in steper layout
   */
  public static class AdapterSteper extends AbstractFragmentStepAdapter {

    private Context mContext;

    public AdapterSteper (
        @NonNull FragmentManager fm,
        @NonNull Context context) {
      super (fm, context);
      this.mContext = context;
    }

    @Override
    public Step createStep (int position) {
      switch (position) {
        case 0:
          return LanggananFragment.newInstance ();
        case 1:
          return PengirimanFragment.newInstance ();
        case 2:
          return PembayaranFragment.newInstance ();
      }
      return null;
    }

    @Override
    public int getCount () {
      return 3;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel (int position) {
      Builder builder = new Builder (context);
      switch (position) {
        case 0:
          builder
              .setTitle ("Mulai")
              .setEndButtonLabel ("Selanjutnya")
              .setBackButtonLabel ("Batal")
              .setNextButtonEndDrawableResId (R.drawable.ic_arrow_forward_white_24dp)
              .setBackButtonStartDrawableResId (StepViewModel.NULL_DRAWABLE);
          break;
        case 1:
          builder
              .setTitle ("Pengiriman")
              .setEndButtonLabel ("Selanjutnya")
              .setBackButtonLabel ("Batal")
              .setNextButtonEndDrawableResId (R.drawable.ic_arrow_forward_white_24dp)
              .setBackButtonStartDrawableResId (StepViewModel.NULL_DRAWABLE);
          break;
        case 2:
          builder
              .setTitle ("Pembayaran")
              .setEndButtonLabel ("Bayar")
              .setBackButtonLabel ("Batal")
              .setNextButtonEndDrawableResId (R.drawable.ic_arrow_forward_white_24dp)
              .setBackButtonStartDrawableResId (StepViewModel.NULL_DRAWABLE);
          break;
      }
      return builder.create ();
    }
  }
}
