package com.simxdeveloper.kulinatest.ui.steper;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.simxdeveloper.kulinatest.R;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout.OnBackClickedCallback;
import com.stepstone.stepper.StepperLayout.OnCompleteClickedCallback;
import com.stepstone.stepper.StepperLayout.OnNextClickedCallback;
import com.stepstone.stepper.VerificationError;


/**
 * A simple {@link Fragment} subclass.
 */
public class PembayaranFragment extends Fragment implements BlockingStep {


  public PembayaranFragment () {
    // Required empty public constructor
  }

  public static PembayaranFragment newInstance () {
    Bundle args = new Bundle ();
    PembayaranFragment fragment = new PembayaranFragment ();
    fragment.setArguments (args);
    return fragment;
  }

  @Override
  public View onCreateView (LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate (R.layout.fragment_pembayaran, container, false);
  }

  @Override
  public void onNextClicked (OnNextClickedCallback callback) {
    callback.goToNextStep ();
  }

  @Override
  public void onCompleteClicked (OnCompleteClickedCallback callback) {
    callback.complete ();
  }

  @Override
  public void onBackClicked (OnBackClickedCallback callback) {
    callback.goToPrevStep ();
  }

  @Nullable
  @Override
  public VerificationError verifyStep () {
    return null;
  }

  @Override
  public void onSelected () {

  }

  @Override
  public void onError (@NonNull VerificationError error) {

  }
}
