package com.simxdeveloper.kulinatest;

import android.app.Application;
import android.content.Context;

/**
 * User: simx Date: 13/05/18 14:06
 */
public class Apps extends Application {
  private static Context context;
  @Override
  public void onCreate () {
    super.onCreate ();
    context = getApplicationContext ();
  }

  public static Context getContext () {
    return context;
  }
}
